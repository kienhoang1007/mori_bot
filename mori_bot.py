from random import randint

from telegram.ext import Updater, CommandHandler
import requests


def get_url():
    contents = requests.get('https://random.dog/woof.json').json()
    url = contents['url']
    return url


def get_girl():
    contents = requests.get('https://api.unsplash.com/search/photos?client_id=W-LEvyOQKo7YxH56P1wUOXfLsEiyMS4IBsAZauUEA_o&query=girl&per_page=100').json()
    random_pic = randint(0, 98)
    url = contents['results'][random_pic]['urls']['small']
    return url


def bop(bot, update):
    url = get_url()
    chat_id = update.message.chat_id
    bot.send_photo(chat_id=chat_id, photo=url)


def girl(bot, update):
    url = get_girl()
    chat_id = update.message.chat_id
    bot.send_photo(chat_id=chat_id, photo=url)


def main():
    updater = Updater('1190096260:AAGKKkypjqbguQ-7MNaBZAGRxMwSqsW7p1U')
    dp = updater.dispatcher
    dp.add_handler(CommandHandler('sam', bop))
    dp.add_handler(CommandHandler('hanni', bop))
    dp.add_handler(CommandHandler('girl', girl))
    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()